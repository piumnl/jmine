package cn.piumnl.se.jmine;

import java.awt.*;

import cn.piumnl.se.jmine.control.MineControl;

/**
 * 启动类
 *
 * @author piumnl
 */
public class StartJmine {
    public static void main(String[] args) {
        EventQueue.invokeLater(MineControl::new);
    }
}
