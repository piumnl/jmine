package cn.piumnl.se.jmine.util;

import java.awt.*;

import javax.swing.*;

/**
 * @author piumnl
 * @version 1.0.0
 * @since on 2017-10-29.
 */
public interface OptionPaneUtil {

    /**
     * 展示消息框
     * @param info 展示的信息
     */
    static void showMessageDialog(String info) {
        JOptionPane.showMessageDialog(null, info);
    }

    /**
     * 展示难度选择框供选择
     * @param msg 显示的信息
     * @param title 标题
     * @return 选择的值
     */
    static Integer showLevelDialog(String msg, String title) {
        return Constant.LEVEL.getOrDefault((String)
                JOptionPane.showInputDialog(null, msg, title, JOptionPane.PLAIN_MESSAGE,
                        null, Constant.ALL_LEVEL, null), 0);
    }

    static int showExitDialog(Component component) {
        return JOptionPane.showConfirmDialog(component, "您确定要退出吗！？", "退出",
                JOptionPane.CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
    }
}
