package cn.piumnl.se.jmine.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author piumnl
 * @version 1.0.0
 * @since on 2017-10-29.
 */
public abstract class Constant {

    // --------------------------难度等级-----------------------------------------
    //初级
    /**
     * 雷区宽度
     */
    public static final int JUNIOR_WIDTH = (1 << 3) + 1;
    /**
     * 雷区高度
     */
    public static final int JUNIOR_HEIGHT = (1 << 3) + 1;
    /**
     * 雷数
     */
    public static final int JUNIOR_MINE = (1 << 3) + (1 << 1);

    //中级
    /**
     * 雷区宽度
     */
    public static final int INTERMEDIATE_WIDTH = 1 << 4;
    /**
     * 雷区高度
     */
    public static final int INTERMEDIATE_HEIGHT = 1 << 4;
    /**
     * 雷数
     */
    public static final int INTERMEDIATE_MINE = (1 << 5) + (1 << 3);

    //高级
    /**
     * 雷区宽度
     */
    public static final int ADVANCED_WIDTH = 1 << 4;
    /**
     * 雷区高度
     */
    public static final int ADVANCED_HEIGHT = (1 << 4) + (1 << 3) + (1 << 2) + (1 << 1);
    /**
     * 雷数
     */
    public static final int ADVANCED_MINE = (1 << 5) + (1 << 6) + (1 << 1) + 1;
    // --------------------------难度等级-----------------------------------------


    public static final String[] ALL_LEVEL = new String[]{"初级", "中级", "高级"};

    public static final Map<String, Integer> LEVEL = new HashMap<>();

    static {
        LEVEL.put(ALL_LEVEL[0], 1);
        LEVEL.put(ALL_LEVEL[1], 2);
        LEVEL.put(ALL_LEVEL[2], 3);
    }
}
