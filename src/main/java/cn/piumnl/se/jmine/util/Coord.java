package cn.piumnl.se.jmine.util;

import java.util.Objects;

/**
 * <p>This class be used of the coordinate.</p>
 * @author piumnl
 * @version 1.0
 */
public class Coord {
    private int x;
    private int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coord() {
        this(0, 0);
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "[x:" + this.x + " y:" + this.y + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        Coord other = (Coord) obj;

        return this.x == other.getX() && this.y == other.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

}
