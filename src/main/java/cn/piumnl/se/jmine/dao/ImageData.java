package cn.piumnl.se.jmine.dao;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.*;

import cn.piumnl.se.jmine.ui.MouseHandler;
import cn.piumnl.se.jmine.util.Constant;
import cn.piumnl.se.jmine.util.Coord;
import cn.piumnl.se.jmine.util.OptionPaneUtil;


public class ImageData {
    public static final ImageIcon COUNT;
    public static final ImageIcon FACE;
    public static final ImageIcon MINE;
    public static final ImageIcon BUTTON;
    public static final ImageIcon[] FLAG = new ImageIcon[3];
    public static final ImageIcon[] NUMBER = new ImageIcon[9];

    public static final int COUNT_EVERY_WIDTH;
    public static final int COUNT_EVERY_HEIGHT;

    static {
        COUNT = new ImageIcon(ImageData.class.getResource("/img/number.png"));
        FACE = new ImageIcon(ImageData.class.getResource("/img/face.png"));
        MINE = new ImageIcon(ImageData.class.getResource("/img/mine_two.png"));
        BUTTON = new ImageIcon(ImageData.class.getResource("/img/button.png"));

        COUNT_EVERY_WIDTH = COUNT.getIconWidth();
        COUNT_EVERY_HEIGHT = COUNT.getIconHeight() / 11;

        NUMBER[0] = new ImageIcon(ImageData.class.getResource("/img/one.png"));
        NUMBER[1] = new ImageIcon(ImageData.class.getResource("/img/two.png"));
        NUMBER[2] = new ImageIcon(ImageData.class.getResource("/img/three.png"));
        NUMBER[3] = new ImageIcon(ImageData.class.getResource("/img/four.png"));
        NUMBER[4] = new ImageIcon(ImageData.class.getResource("/img/five.png"));
        NUMBER[5] = new ImageIcon(ImageData.class.getResource("/img/six.png"));
        NUMBER[6] = new ImageIcon(ImageData.class.getResource("/img/seven.png"));
        NUMBER[7] = new ImageIcon(ImageData.class.getResource("/img/eight.png"));
        NUMBER[8] = new ImageIcon(ImageData.class.getResource("/img/mine_one.png"));

        FLAG[0] = BUTTON;
        FLAG[1] = new ImageIcon(ImageData.class.getResource("/img/flag.png"));
        FLAG[2] = new ImageIcon(ImageData.class.getResource("/img/question.png"));
    }


    public static final int FLAG_NUM = 1;  //旗帜的代号
    public static final int QUESTION_NUM = 2;  //问题的代号
    private static final int MINE_NUM = 9;  //雷的代号
    private static final int BORDER = 10;  //边框的代号

    //当前
    private int currentWidth;
    private int currentHeight;
    private int currentMine;

    /**
     * 记录剩余雷的个数
     */
    private int count = 0;
    /**
     * 记录当前状态
     */
    private boolean status = true;
    /**
     * 记录游戏时间
     */
    private int time = 0;
    /**
     * 当前点击数
     */
    private int click = 0;

    private boolean check[][] = null;  //检测状态。

    private int[][] game = null;  //雷区的具体实现。
    private List<Coord> flag = null;
    private boolean live = false;
    private Map<Integer, Integer> mines = null;
    private JButton[][] button = null;  //雷区的表现

    public ImageData(int level) {
        this.click = 0;
        this.status = true;
        this.time = 0;

        // 通过选择的难度来决定初始化当前雷区的宽度，长度，雷的个数
        initialCurrent(level);

        this.count = currentMine;
        this.check = new boolean[currentWidth + 2][currentHeight + 2];
        this.mines = new HashMap<>();
        this.flag = new ArrayList<>();
        this.game = new int[currentWidth + 2][currentHeight + 2];
        this.button = new JButton[currentWidth][currentHeight];

        initalGameCheck();   //初始化game变量和check变量。
        initialMine();  //初始化雷的坐标。
        initialGameMap();  //遍历所有区域，初始化地图
        initialButtons();  //初始化buttons相关数据。
    }

    private void initialCurrent(int level) {
        switch (level) {
            case 1:
                currentWidth = Constant.JUNIOR_WIDTH;
                currentHeight = Constant.JUNIOR_HEIGHT;
                currentMine = Constant.JUNIOR_MINE;
                break;
            case 2:
                currentWidth = Constant.INTERMEDIATE_WIDTH;
                currentHeight = Constant.INTERMEDIATE_HEIGHT;
                currentMine = Constant.INTERMEDIATE_MINE;
                break;
            case 3:
                currentWidth = Constant.ADVANCED_WIDTH;
                currentHeight = Constant.ADVANCED_HEIGHT;
                currentMine = Constant.ADVANCED_MINE;
                break;
            default:
                currentWidth = 0;
                currentHeight = 0;
                currentMine = 0;
                OptionPaneUtil.showMessageDialog("游戏初始化失败！");
                System.exit(-1);
        }
    }

    /**
     * 初始化所有Button，添加监听器
     */
    private void initialButtons() {
        for (int i = 0; i < button.length; i++) {//30
            for (int j = 0; j < button[i].length; j++) { //16
                this.button[i][j] = new JButton(BUTTON);
                this.button[i][j].addMouseListener(new MouseHandler(this, i + 1, j + 1, this.game[i + 1][j + 1]));
            }
        }
    }

    /**
     * 初始化雷的坐标
     */
    private void initialMine() {
        int mines = 0;

        Random r = new Random();  //获取随机数。
        while (mines < currentMine)  //埋雷。
        {
            int x = r.nextInt(this.game.length - 2) + 1;
            int y = r.nextInt(this.game[0].length - 2) + 1;
            if (this.game[x][y] == MINE_NUM) {
                continue;
            }
            this.game[x][y] = MINE_NUM;
            this.mines.put(x, y);
            mines++;
        }
    }

    /**
     * 初始化游戏地图。
     */
    private void initialGameMap() {
        for (int i = 1; i < this.game.length - 1; i++) {
            for (int j = 1; j < this.game[i].length - 1; j++) {
                if (this.game[i][j] == MINE_NUM) {
                    //如果当前就是雷区，不用遍历周围。
                    continue;
                }

                for (int n = -1; n < 2; n++) {//不是雷区，需要遍历。
                    for (int m = -1; m < 2; m++) {
                        if (this.game[i + n][j + m] == MINE_NUM) {
                            this.game[i][j]++;
                        }
                    }
                }
            }
        }
    }

    /**
     * 设置game和check的初始化属性
     */
    private void initalGameCheck() {
        //将雷区清零
        for (int i = 1; i < this.game.length - 2; i++) {
            for (int j = 1; j < this.game[i].length - 2; j++) {
                this.game[i][j] = 0;
                this.check[i - 1][j - 1] = false;
            }
        }
        //设置边框和检查状态
        for (int i = 0; i < this.game.length; i++) {
            this.game[i][0] = BORDER;
            this.game[i][this.game[i].length - 1] = BORDER;
            this.check[i][0] = true;
            this.check[i][this.game[i].length - 1] = true;
        }
        for (int i = 0; i < this.game[0].length; i++) {
            this.game[0][i] = BORDER;
            this.game[this.game.length - 1][i] = BORDER;
            this.check[0][i] = true;
            this.check[this.game.length - 1][i] = true;
        }
    }

    /**
     * 绘制数字
     *
     * @param number 要绘制的数字
     * @param g      画笔
     */
    public static void paintNumber(int number, Graphics g) {
        StringBuilder strNum = new StringBuilder(String.valueOf(number));
        while (strNum.length() < 3) {
            strNum.insert(0, '0');
        }

        for (int i = 0; i < 3; i++) {
            int num = strNum.charAt(i) - '0';
            g.drawImage(COUNT.getImage(), i * COUNT_EVERY_WIDTH, 0, (i + 1) * COUNT_EVERY_WIDTH, COUNT_EVERY_HEIGHT,
                    0, num * COUNT_EVERY_HEIGHT, COUNT_EVERY_WIDTH, (num + 1) * COUNT_EVERY_HEIGHT, null);
        }
    }

    public void addClick() {
        this.click++;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int[][] getGame() {
        return game;
    }

    public Map<Integer, Integer> getMines() {
        return mines;
    }

    public JButton[][] getButton() {
        return button;
    }

    public int getCurrentWidth() {
        return currentWidth;
    }

    public int getCurrentHeight() {
        return currentHeight;
    }

    public int getCurrentMine() {
        return currentMine;
    }

    public boolean[][] getCheck() {
        return check;
    }

    public List<Coord> getFlag() {
        return flag;
    }

    public void setFlag(Coord flag) {
        this.flag.add(flag);
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public int getClick() {
        return click;
    }

}
