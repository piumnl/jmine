package cn.piumnl.se.jmine.ui;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.SoftBevelBorder;

import cn.piumnl.se.jmine.dao.ImageData;
import cn.piumnl.se.jmine.ui.panel.GamePanel;
import cn.piumnl.se.jmine.ui.panel.ViewPanel;

public class MainPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int PADDING = 5;

    private ImageData data = null;

    public MainPanel(ImageData data) {
        this.data = data;

        //设置外边框。
        Border border1 = new SoftBevelBorder(BevelBorder.RAISED, Color.WHITE, Color.GRAY);
        this.setBorder(border1);

        //设置内布局
        setLayout();

        //杂项设置
        setBackground(Color.GRAY);
    }

    /**
     * 对面板的布局处理。
     */
    private void setLayout() {
        this.setLayout(new BorderLayout(10, 5));  //设置主面板的布局 ----边框布局。

        //设置北面面板和中心面板
        ViewPanel view = new ViewPanel(data); //添加视图面板到主面板
        view.setPreferredSize(new Dimension(ImageData.NUMBER[0].getIconWidth() * data.getCurrentWidth(), ImageData.COUNT.getIconHeight() / 11 + PADDING));

        GamePanel center = new GamePanel(data); //添加游戏面板到主面板
        center.setPreferredSize(new Dimension(ImageData.NUMBER[0].getIconHeight() * data.getCurrentHeight(), ImageData.NUMBER[0].getIconWidth() * data.getCurrentWidth()));

        this.add(view, BorderLayout.NORTH);
        this.add(center, BorderLayout.CENTER);
    }

    public void setData(ImageData data) {
        this.data = data;
    }
}
