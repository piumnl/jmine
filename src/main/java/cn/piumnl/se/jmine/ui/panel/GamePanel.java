package cn.piumnl.se.jmine.ui.panel;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.Border;

import cn.piumnl.se.jmine.dao.ImageData;

/**
 * 设置雷区面板
 *
 * @author piumnl
 */
public class GamePanel extends JPanel {
    private static final long serialVersionUID = 1L;

    public GamePanel(ImageData data) {
        //设置布局 ---边框布局
        this.setLayout(new GridLayout(data.getCurrentWidth(), data.getCurrentHeight()));

        //设置边框  --凹面边框
        Border centerBorder = BorderFactory.createLoweredBevelBorder();
        this.setBorder(centerBorder);

        //将按钮添加到面板。
        JButton[][] buttons = data.getButton();
        for (JButton[] button : buttons) {
            for (JButton btn : button) {
                this.add(btn);
            }
        }
    }
}
