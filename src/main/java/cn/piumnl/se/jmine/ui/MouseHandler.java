package cn.piumnl.se.jmine.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;

import cn.piumnl.se.jmine.control.MineControl;
import cn.piumnl.se.jmine.dao.ImageData;
import cn.piumnl.se.jmine.util.Coord;

public class MouseHandler extends MouseAdapter {
    private final int x;
    private final int y;
    private final int status;
    private int flag = 0;
    private ImageData data;
    private TimerTask task;
    private static MineControl control;
    private static boolean check[][] = null;

    public MouseHandler(ImageData data, int x, int y, int status) {
        this.x = x;
        this.y = y;
        this.status = status;
        this.data = data;
    }

    public static void setControl(MineControl control) {
        MouseHandler.control = control;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        check = data.getCheck();
        JButton source = (JButton) event.getSource();

        if (!source.isEnabled()) {
            return;
        }

        if (event.getButton() == MouseEvent.BUTTON1) {
            leftMouseClick(event, source);
        } else if (event.getButton() == MouseEvent.BUTTON3 && !check[x][y]) {
            rightMouseClick(event, source);
        }
    }

    /**
     * 鼠标右键被按下的处理。
     */
    private void rightMouseClick(MouseEvent event, JButton source) {
        flag++;
        flag = flag % 3;
        source.setIcon(ImageData.FLAG[flag]);

        if (flag == ImageData.FLAG_NUM) {
            data.setCount(data.getCount() - 1);
            List<Coord> coords = data.getFlag();
            coords.add(new Coord(x, y));

            if (data.getCount() == 0) {
                control.flagEqualsMine();
            }

        } else if (flag == ImageData.QUESTION_NUM) {
            data.setCount(data.getCount() + 1);
        }

        control.repaint();  //必不可少，用于刷新界面。
    }

    /**
     * 鼠标左键被按下的处理。
     */
    private void leftMouseClick(MouseEvent event, JButton source) {

        if (!data.isLive()) {
            timeTask();
        }
        check[x][y] = true;
        data.addClick();
        if (status == 0) {
            traversalEmpty();
            return;
        }

        source.setBorderPainted(false);   //去除焦点边框。
        source.setIcon(ImageData.NUMBER[status - 1]);

        if (status == 9) {
            control.flagEqualsMine();
        }
    }

    /**
     * 遍历空位置。
     */
    private void traversalEmpty() {
        Stack<Coord> stack = new Stack<>();  //栈

        //入栈操作
        pushElement(stack, x, y);
        if (stack.isEmpty()) {
            return;
        }

        //开始遍历
        traversalStack(stack);
    }

    private void traversalStack(Stack<Coord> stack) {
        Coord top = stack.pop();
        int Coord_x = top.getX();    //当前检测位置的坐标
        int Coord_y = top.getY();

        //仅当当前检测的位置为未检测状态，且当前位置的状态为0，方可继续。
        if (!check[Coord_x][Coord_y] && data.getGame()[Coord_x][Coord_y] == 0) {
            pushElement(stack, Coord_x, Coord_y);
        }

        if (!stack.isEmpty()) {
            traversalStack(stack);
        }
    }

    private void pushElement(Stack<Coord> stack, int Coord_x, int Coord_y) {
        //设置当前位置的状态
        check[Coord_x][Coord_y] = true;
        //设置当前按钮禁用状态。
        data.getButton()[Coord_x - 1][Coord_y - 1].setEnabled(false);
        //检测周边是否被检测过。
        if (!check[Coord_x - 1][Coord_y] && data.getGame()[Coord_x - 1][Coord_y] == 0) {
            stack.push(new Coord(Coord_x - 1, Coord_y));
        }
        if (!check[Coord_x + 1][Coord_y] && data.getGame()[Coord_x + 1][Coord_y] == 0) {
            stack.push(new Coord(Coord_x + 1, Coord_y));
        }
        if (!check[Coord_x][Coord_y - 1] && data.getGame()[Coord_x][Coord_y - 1] == 0) {
            stack.push(new Coord(Coord_x, Coord_y - 1));
        }
        if (!check[Coord_x][Coord_y + 1] && data.getGame()[Coord_x][Coord_y + 1] == 0) {
            stack.push(new Coord(Coord_x, Coord_y + 1));
        }
    }

    private void timeTask() {
        data.setLive(true);
        Timer timer = new Timer("timer");
        this.task = new TimerTask() {
            @Override
            public void run() {
                data.setTime(data.getTime() + 1);
                control.repaint();
            }
        };
        timer.schedule(task, 1000, 1000L);
    }

    public void cancelTask() {
        this.task.cancel();
    }
}
