package cn.piumnl.se.jmine.ui.panel;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.Border;

import cn.piumnl.se.jmine.dao.ImageData;

/**
 * 设置视图面板。
 *
 * @author piumnl
 */
public class ViewPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private ImageData data = null;
    public static final int COUNT = 3;

    public ViewPanel(ImageData data) {
        this.data = data;

        //设置布局 ---边框布局
        this.setLayout(new BorderLayout());
        //设置边框  --凹面边框
        Border northBorder = BorderFactory.createLoweredBevelBorder();
        this.setBorder(northBorder);

        //设置三个方位面板
        setWest();
        setCenter();
        setEast();
    }

    /**
     * 显示剩余雷区数。
     */
    private void setWest() {
        @SuppressWarnings("serial")
        JPanel west = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                ImageData.paintNumber(data.getCount(), g);
            }
        };

        west.setPreferredSize(new Dimension(ImageData.COUNT.getIconWidth() * COUNT, ImageData.COUNT.getIconHeight()));
        this.add(west, BorderLayout.WEST);
    }

    /**
     * 显示状态
     */
    private void setCenter() {
        @SuppressWarnings("serial")
        JPanel west = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                if (data.isStatus()) {
                    g.drawImage(ImageData.FACE.getImage(), (this.getWidth() >> 1) - 12, 0, (this.getWidth() >> 1) + 12, 24, 0, 0, 24, 24, null);
                } else {
                    g.drawImage(ImageData.FACE.getImage(), (this.getWidth() >> 1) - 12, 0, (this.getWidth() >> 1) + 12, 24, 0, 48, 24, 72, null);
                }
            }
        };
        this.add(west, BorderLayout.CENTER);
    }

    /**
     * 显示时间
     */
    private void setEast() {
        @SuppressWarnings("serial")
        JPanel east = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                ImageData.paintNumber((int) data.getTime(), g);
            }
        };
        east.setPreferredSize(new Dimension(ImageData.COUNT.getIconWidth() * COUNT, ImageData.COUNT.getIconHeight()));
        this.add(east, BorderLayout.EAST);
    }
}
