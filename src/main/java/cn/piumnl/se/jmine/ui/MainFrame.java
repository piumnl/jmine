package cn.piumnl.se.jmine.ui;

import java.awt.*;

import javax.swing.*;

public class MainFrame extends JFrame {
    private static final long serialVersionUID = 1L;

    public MainFrame(MainPanel mainPanel, MainMenuBar menuBar) {

        //设置标题
        this.setTitle("扫雷");
        setParam(mainPanel, menuBar);

        //设置Frame在屏幕中的位置
        this.setScreenCenter();
        this.setResizable(false);  //设置是否可以改变大小。
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //设置默认的退出方式。

        this.setVisible(true);  //设置可见。
        this.pack();
    }

    public void setParam(MainPanel mainPanel, MainMenuBar menuBar) {
        this.setContentPane(mainPanel);  //添加主面板
        this.setJMenuBar(menuBar);  //设置菜单栏。
    }

    /**
     * 设置窗口居中
     */
    private void setScreenCenter() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screen = toolkit.getScreenSize();
        int x = (screen.width - 200) / 2;
        int y = (screen.height - 200) / 2;
        this.setLocation(x, y);
    }
}
