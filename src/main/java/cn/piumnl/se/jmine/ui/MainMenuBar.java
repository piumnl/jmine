package cn.piumnl.se.jmine.ui;

import javax.swing.*;

import cn.piumnl.se.jmine.control.MineControl;
import cn.piumnl.se.jmine.util.OptionPaneUtil;

/**
 * 处理菜单栏所有事情。
 *
 * @author piumnl
 */
public class MainMenuBar extends JMenuBar {
    private static final long serialVersionUID = 1L;
    private MineControl control = null;

    public MainMenuBar(MineControl control) {
        this.control = control;
    }

    /**
     * 添加所需的所有菜单到frame框架中。
     */
    public void setFrame(JFrame frame) {
        //create all menu.
        JMenu gameMenu = getGameMenu("game");
        JMenu plafMenu = getLookFeelMenu("观感", frame);

        //add menu to menuBar.
        this.add(gameMenu);
        this.add(plafMenu);
    }

    private JMenu getGameMenu(String name) {
        JMenu game = new JMenu(name);

        //add menu item to 'game' menu.
        JMenuItem easy = new JMenuItem("初级");
        JMenuItem middling = new JMenuItem("中级");
        JMenuItem difficult = new JMenuItem("高级");
        JMenuItem exitMenu = new JMenuItem("退出");

        easy.addActionListener(e -> control.initialGame());
        middling.addActionListener(e -> control.initialGame());
        difficult.addActionListener(e -> control.initialGame());
        exitMenu.addActionListener(e -> {
            if (OptionPaneUtil.showExitDialog(control.getFrame()) == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        });

        game.add(easy);
        game.add(middling);
        game.add(difficult);
        game.addSeparator();
        game.add(exitMenu);

        return game;
    }

    /**
     * 设置观感
     *
     * @param frame 观感依赖于一个根。
     */
    private JMenu getLookFeelMenu(String name, JFrame frame) {
        JMenu menu = new JMenu(name);  //设置观感的菜单栏名称。
        JMenuItem[] items;   //创建观感菜单的子菜单。

        UIManager.LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();  //获得当前系统中可使用的所有观感。
        items = new JMenuItem[infos.length];  //创建指定个子菜单。
        for (int i = 0; i < infos.length; i++)  //对子菜单的处理。
        {
            items[i] = makeMenuItem(infos[i].getName(), infos[i].getClassName(), frame);
            menu.add(items[i]);  //将子菜单添加到观感的菜单栏中。
        }

        return menu;
    }

    /**
     * 创建一个观感子菜单，并为其添加监听
     *
     * @param name     子菜单显式名称
     * @param plafName 观感名称
     * @param frame    依赖的框架。
     */
    private JMenuItem makeMenuItem(String name, final String plafName, final JFrame frame) {
        JMenuItem menuItem = new JMenuItem(name);  //创建一个子菜单

        menuItem.addActionListener(event -> {
            try {
                UIManager.setLookAndFeel(plafName);  //设置当前框架的观感。
                SwingUtilities.updateComponentTreeUI(frame);   //更新当前框架的观感。
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return menuItem;
    }
}

