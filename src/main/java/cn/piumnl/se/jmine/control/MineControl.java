package cn.piumnl.se.jmine.control;

import java.awt.*;
import java.util.List;
import java.util.Map;

import cn.piumnl.se.jmine.dao.ImageData;
import cn.piumnl.se.jmine.ui.MainFrame;
import cn.piumnl.se.jmine.ui.MainMenuBar;
import cn.piumnl.se.jmine.ui.MainPanel;
import cn.piumnl.se.jmine.ui.MouseHandler;
import cn.piumnl.se.jmine.util.Coord;
import cn.piumnl.se.jmine.util.OptionPaneUtil;

public class MineControl {
    private MainFrame frame = null;

    private MainPanel panel = null;
    private MainMenuBar bar = null;

    private ImageData data = null;

    public MineControl() {
        this.data = new ImageData(OptionPaneUtil.showLevelDialog("请选择难度", "扫雷"));
        this.panel = new MainPanel(this.data);
        this.bar = new MainMenuBar(this);
        this.frame = new MainFrame(this.panel, this.bar);
        this.bar.setFrame(this.frame);

        MouseHandler.setControl(this);
    }

    public void startGame() {
        initialGame();
    }

    public void initialGame() {
        this.frame.setVisible(false);
        this.data = new ImageData(OptionPaneUtil.showLevelDialog("请选择难度", "扫雷"));
        this.panel = new MainPanel(this.data);
        this.bar = new MainMenuBar(this);
        this.frame = new MainFrame(this.panel, this.bar);
        this.bar.setFrame(this.frame);
        this.panel.repaint();
    }

    public void repaint() {
        this.panel.repaint();
    }

    /**
     * 判断是否胜利或失败。
     */
    public void flagEqualsMine() {
        List<Coord> flag = data.getFlag();
        Map<Integer, Integer> mines = data.getMines();

        boolean win = false;
        int count = 0;
        for (Coord aFlag : flag) {
            int result = mines.get(aFlag.getX());
            if (aFlag.getY() == result) {
                win = true;
                count++;
            }
        }

        if (win) {
            gameWin();
        } else {
            gameOver(count);
        }

        startGame();
        panel.repaint();
    }

    /**
     * 游戏获胜
     */
    public void gameWin() {
        String title = "恭喜你赢了！\n";
        String strTime = "您花时" + data.getTime() + "秒\n";
        String strCount = "您共点击了" + data.getClick() + "次";

        String information = title.concat(strTime).concat(strCount);

        OptionPaneUtil.showMessageDialog(information);
    }

    /**
     * 游戏失败
     */
    public void gameOver(int count) {
        this.data.setStatus(false);
        String title = "游戏结束\n";
        String strTime = "您花时" + data.getTime() + "秒\n";
        String strCount = "您共扫了" + count + "雷。";

        String information = title.concat(strTime).concat(strCount);

        OptionPaneUtil.showMessageDialog(information);
    }

    public Component getFrame() {
        return this.frame;
    }
}
